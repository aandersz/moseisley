package com.lemonzombie.moseisley.managers;

import com.lemonzombie.moseisley.events.SearchResponseError;
import com.lemonzombie.moseisley.network.EndpointService;
import com.lemonzombie.moseisley.utils.L;
import com.lemonzombie.moseisley.utils.RxBus;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Adrian on 28.01.2016.
 */
@Singleton
public class RestManager {
    public static final String TAG = RestManager.class.getSimpleName();

    RxBus rxBus;
    EndpointService endpointService;
    Map<String, String> requestMap;

    @Inject
    public RestManager(EndpointService endpointService,
                       RxBus rxBus){
        this.endpointService = endpointService;
        this.rxBus = rxBus;

        requestMap = new HashMap<>();
    }

    public void search (Map<String, String> requestMap){

        endpointService.search(requestMap)
                .onErrorReturn(throwable -> {
                    L.e(TAG, "Error: " + throwable.getMessage());
                    rxBus.send(new SearchResponseError(throwable.getMessage()));
                    return null;
                })
                .subscribe(rxBus::send);
    }

}
