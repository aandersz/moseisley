package com.lemonzombie.moseisley.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.lemonzombie.moseisley.R;
import com.lemonzombie.moseisley.models.SearchItem;

import java.util.List;

/**
 * Created by Adrian on 30.01.2016.
 */
public class SearchVenuesAdapter extends ArrayAdapter<SearchItem> {
    private static final String TAG = SearchVenuesAdapter.class.getSimpleName();

    List<SearchItem> objects;
    public SearchVenuesAdapter(Context context, int resource, List<SearchItem> objects) {
        super(context, resource, objects);
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;
        SearchItem item = objects.get(position);
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.search_item, null);
            holder = new ViewHolder();
            holder.name = (TextView) v.findViewById(R.id.search_item_name);
            holder.address = (TextView) v.findViewById(R.id.search_item_address);
            holder.distance = (TextView) v.findViewById(R.id.search_item_distance);

            holder.name.setText(item.getName());
            holder.address.setText(item.getAddress());
            holder.distance.setText(item.getDistance());

            v.setTag(holder);
        }
        else {
            holder = (ViewHolder) v.getTag();
        }

        return v;
    }

    private static class ViewHolder{
        private TextView name;
        private TextView address;
        private TextView distance;
    }
}
