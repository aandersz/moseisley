package com.lemonzombie.moseisley.network;

import com.lemonzombie.moseisley.models.network.SearchResponse;

import java.util.Map;

import retrofit.http.GET;
import retrofit.http.Query;
import retrofit.http.QueryMap;
import rx.Observable;

/**
 * Created by Adrian on 27.01.2016.
 */
public interface EndpointService {
    static final String URL = "https://api.foursquare.com";
    static final String CLIENT_ID = "client_id";
    static final String CLIENT_SECRET = "client_secret";
    static final String LAT_LON = "ll";
    static final String VERSION = "v";
    static final String QUERY = "query";

    @GET("/v2/venues/search")
    Observable<SearchResponse> search(@Query(CLIENT_ID) String clientId,
                                      @Query(CLIENT_SECRET) String clientSecret,
                                      @Query(LAT_LON) String latLon,
                                      @Query(VERSION) String version,
                                      @Query(QUERY) String query);

    @GET("/v2/venues/search")
    Observable<SearchResponse> search(@QueryMap Map map);
}
