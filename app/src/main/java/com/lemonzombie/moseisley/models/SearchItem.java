package com.lemonzombie.moseisley.models;

/**
 * Created by Adrian on 30.01.2016.
 */
public class SearchItem {
    String name;
    String address;
    String distance;

    public SearchItem(String name, String address, String distance) {
        this.name = name;
        this.address = address;
        this.distance = distance;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getDistance() {
        return distance;
    }
}
