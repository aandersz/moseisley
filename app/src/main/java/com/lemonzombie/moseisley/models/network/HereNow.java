package com.lemonzombie.moseisley.models.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adrian on 28.01.2016.
 */
public class HereNow {

    @SerializedName("count")
    @Expose
    public Integer count;
    @SerializedName("summary")
    @Expose
    public String summary;
    @SerializedName("groups")
    @Expose
    public List<Object> groups = new ArrayList<Object>();

    public Integer getCount() {
        return count;
    }

    public String getSummary() {
        return summary;
    }

    public List<Object> getGroups() {
        return groups;
    }
}