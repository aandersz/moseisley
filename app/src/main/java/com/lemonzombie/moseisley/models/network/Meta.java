package com.lemonzombie.moseisley.models.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Adrian on 28.01.2016.
 */
public class Meta {

    @SerializedName("code")
    @Expose
    public Integer code;
    @SerializedName("requestId")
    @Expose
    public String requestId;

    public Integer getCode() {
        return code;
    }

    public String getRequestId() {
        return requestId;
    }
}