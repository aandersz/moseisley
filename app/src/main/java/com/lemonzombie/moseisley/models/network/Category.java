package com.lemonzombie.moseisley.models.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created by Adrian on 28.01.2016.
 */

public class Category {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("pluralName")
    @Expose
    public String pluralName;
    @SerializedName("shortName")
    @Expose
    public String shortName;
    @SerializedName("icon")
    @Expose
    public Icon icon;
    @SerializedName("primary")
    @Expose
    public Boolean primary;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPluralName() {
        return pluralName;
    }

    public String getShortName() {
        return shortName;
    }

    public Icon getIcon() {
        return icon;
    }

    public Boolean getPrimary() {
        return primary;
    }
}