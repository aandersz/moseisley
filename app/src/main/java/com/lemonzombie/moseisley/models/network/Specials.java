package com.lemonzombie.moseisley.models.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adrian on 28.01.2016.
 */
public class Specials {

    @SerializedName("count")
    @Expose
    public Integer count;
    @SerializedName("items")
    @Expose
    public List<Object> items = new ArrayList<Object>();

    public Integer getCount() {
        return count;
    }

    public List<Object> getItems() {
        return items;
    }
}
