package com.lemonzombie.moseisley.models.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Adrian on 28.01.2016.
 */
public class Menu {

    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("label")
    @Expose
    public String label;
    @SerializedName("anchor")
    @Expose
    public String anchor;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("mobileUrl")
    @Expose
    public String mobileUrl;
    @SerializedName("externalUrl")
    @Expose
    public String externalUrl;

    public String getType() {
        return type;
    }

    public String getLabel() {
        return label;
    }

    public String getAnchor() {
        return anchor;
    }

    public String getUrl() {
        return url;
    }

    public String getMobileUrl() {
        return mobileUrl;
    }

    public String getExternalUrl() {
        return externalUrl;
    }
}