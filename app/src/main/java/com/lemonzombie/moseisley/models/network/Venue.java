package com.lemonzombie.moseisley.models.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adrian on 28.01.2016.
 */
public class Venue {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("contact")
    @Expose
    public Contact contact;
    @SerializedName("location")
    @Expose
    public Location location;
    @SerializedName("categories")
    @Expose
    public List<Category> categories = new ArrayList<Category>();
    @SerializedName("verified")
    @Expose
    public Boolean verified;
    @SerializedName("stats")
    @Expose
    public Stats stats;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("allowMenuUrlEdit")
    @Expose
    public Boolean allowMenuUrlEdit;
    @SerializedName("specials")
    @Expose
    public Specials specials;
    @SerializedName("hereNow")
    @Expose
    public HereNow hereNow;
    @SerializedName("referralId")
    @Expose
    public String referralId;
    @SerializedName("venueChains")
    @Expose
    public List<Object> venueChains = new ArrayList<Object>();
    @SerializedName("storeId")
    @Expose
    public String storeId;
    @SerializedName("menu")
    @Expose
    public Menu menu;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Contact getContact() {
        return contact;
    }

    public Location getLocation() {
        return location;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public Boolean getVerified() {
        return verified;
    }

    public Stats getStats() {
        return stats;
    }

    public String getUrl() {
        return url;
    }

    public Boolean getAllowMenuUrlEdit() {
        return allowMenuUrlEdit;
    }

    public Specials getSpecials() {
        return specials;
    }

    public HereNow getHereNow() {
        return hereNow;
    }

    public String getReferralId() {
        return referralId;
    }

    public List<Object> getVenueChains() {
        return venueChains;
    }

    public String getStoreId() {
        return storeId;
    }

    public Menu getMenu() {
        return menu;
    }
}
