package com.lemonzombie.moseisley.models.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adrian on 28.01.2016.
 */
public class Location {

    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("lat")
    @Expose
    public Double lat;
    @SerializedName("lng")
    @Expose
    public Double lng;
    @SerializedName("distance")
    @Expose
    public Integer distance;
    @SerializedName("cc")
    @Expose
    public String cc;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("state")
    @Expose
    public String state;
    @SerializedName("country")
    @Expose
    public String country;
    @SerializedName("formattedAddress")
    @Expose
    public List<String> formattedAddress = new ArrayList<String>();

    public String getAddress() {
        return address;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLng() {
        return lng;
    }

    public Integer getDistance() {
        return distance;
    }

    public String getCc() {
        return cc;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getCountry() {
        return country;
    }

    public List<String> getFormattedAddress() {
        return formattedAddress;
    }
}