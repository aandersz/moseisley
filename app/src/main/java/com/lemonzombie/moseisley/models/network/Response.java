package com.lemonzombie.moseisley.models.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adrian on 28.01.2016.
 */
public class Response {

    @SerializedName("venues")
    @Expose
    public List<Venue> venues = new ArrayList<Venue>();

    public List<Venue> getVenues() {
        return venues;
    }
}