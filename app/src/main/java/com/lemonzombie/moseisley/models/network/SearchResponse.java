package com.lemonzombie.moseisley.models.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Adrian on 27.01.2016.
 */
public class SearchResponse {

    @SerializedName("meta")
    @Expose
    public Meta meta;
    @SerializedName("response")
    @Expose
    public Response response;

    public Meta getMeta() {
        return meta;
    }

    public Response getResponse() {
        return response;
    }
}