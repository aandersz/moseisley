package com.lemonzombie.moseisley.models.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Adrian on 28.01.2016.
 */
public class Stats {

    @SerializedName("checkinsCount")
    @Expose
    public Integer checkinsCount;
    @SerializedName("usersCount")
    @Expose
    public Integer usersCount;
    @SerializedName("tipCount")
    @Expose
    public Integer tipCount;

    public Integer getCheckinsCount() {
        return checkinsCount;
    }

    public Integer getUsersCount() {
        return usersCount;
    }

    public Integer getTipCount() {
        return tipCount;
    }
}