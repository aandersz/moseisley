package com.lemonzombie.moseisley;

import android.app.Application;

/**
 * Created by Adrian on 12.12.2015.
 */
public class App extends Application {
    private static final String TAG = App.class.getSimpleName();

    public AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = AppComponent.Initializer.init(this);
        appComponent.inject(this);

    }

    public AppComponent getComponent(){
        return appComponent;
    }

}
