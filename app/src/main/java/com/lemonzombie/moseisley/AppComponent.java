package com.lemonzombie.moseisley;

import com.lemonzombie.moseisley.activites.BaseActivity;
import com.lemonzombie.moseisley.activites.MainActivity;
import com.lemonzombie.moseisley.fragments.BaseFragment;
import com.lemonzombie.moseisley.fragments.MainFragment;
import com.lemonzombie.moseisley.managers.RestManager;
import com.lemonzombie.moseisley.receivers.LocationServicesChangeReceiver;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Adrian on 12.12.2015.
 */
@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    void inject(App application);
    void inject(BaseActivity activity);
    void inject(MainActivity activity);

    void inject(BaseFragment fragment);
    void inject(MainFragment fragment);

    void inject(LocationServicesChangeReceiver receiver);

    void inject(RestManager manager);

    final class Initializer {

        public static AppComponent init(App app) {
            return DaggerAppComponent.builder()
                    .appModule(new AppModule(app))
                    .build();
        }
    }
}
