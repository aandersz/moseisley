package com.lemonzombie.moseisley.activites;

import android.app.Activity;
import android.os.Bundle;

import com.lemonzombie.moseisley.App;
import com.lemonzombie.moseisley.utils.RxBus;

import javax.inject.Inject;

/**
 * Created by Adrian on 13.12.2015.
 */
public class BaseActivity extends Activity {
    private static final String TAG = BaseActivity.class.getSimpleName();

    @Inject
    protected RxBus rxBus;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App)getApplication()).getComponent().inject(this);

    }


}
