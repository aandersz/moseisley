package com.lemonzombie.moseisley.activites;

import android.Manifest;
import android.app.FragmentManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.lemonzombie.moseisley.App;
import com.lemonzombie.moseisley.R;
import com.lemonzombie.moseisley.events.ReceivedLocationEvent;
import com.lemonzombie.moseisley.fragments.MainFragment;
import com.lemonzombie.moseisley.managers.RestManager;
import com.lemonzombie.moseisley.utils.L;

import javax.inject.Inject;

public class MainActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = MainActivity.class.getSimpleName();

    protected Location lastLocation;

    @Inject
    GoogleApiClient googleApiClient;

    @Inject
    RestManager restManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((App)getApplication()).getComponent().inject(this);

        FragmentManager fm = getFragmentManager();
        MainFragment fragment = new MainFragment();
        fm.beginTransaction().replace(R.id.main_content, fragment).commit();

        googleApiClient.registerConnectionCallbacks(this);
        googleApiClient.registerConnectionFailedListener(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: check this with Android M
                return;
            }
        }

        lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        if (lastLocation != null) {
            rxBus.send(new ReceivedLocationEvent(lastLocation));
        } else {
            LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            String locationProvider = LocationManager.NETWORK_PROVIDER;
            Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);

            if(lastKnownLocation != null){
                rxBus.send(new ReceivedLocationEvent(lastKnownLocation));
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        L.w(TAG, "Connection suspended");
        googleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        L.e(TAG, "Connection failed ERROR CODE: " + connectionResult.getErrorCode());
    }

}
