package com.lemonzombie.moseisley.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.lemonzombie.moseisley.App;
import com.lemonzombie.moseisley.events.ReceivedLocationEvent;
import com.lemonzombie.moseisley.utils.L;
import com.lemonzombie.moseisley.utils.RxBus;

import javax.inject.Inject;

/**
 * Created by Adrian on 30.01.2016.
 */
public class LocationServicesChangeReceiver extends BroadcastReceiver {
    public static final String TAG = LocationServicesChangeReceiver.class.getSimpleName();

    @Inject
    RxBus rxBus;

    @Inject
    GoogleApiClient googleApiClient;

    @Inject
    Context context;

    LocationManager locationManager;

    @Override
    public void onReceive(Context context, Intent intent) {
        ((App) context.getApplicationContext()).getComponent().inject(this);

        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if (intent.getAction() != null) {
            L.d(TAG, "Localization receiver action: " + intent.getAction());
            switch (intent.getAction()) {
                case LocationManager.MODE_CHANGED_ACTION:
                    L.d(TAG, "MODE CHANGED");
                case LocationManager.PROVIDERS_CHANGED_ACTION:
                    L.d(TAG, "PROVIDERS CHANGED");
                    if (isLocationEnabled()) {
                        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

                        L.d(TAG, "LastLocation " + lastLocation);
                        L.d(TAG, "google api client " + googleApiClient.isConnected());

                        if(lastLocation != null){
                            rxBus.send(new ReceivedLocationEvent(lastLocation));
                        }
                    }
                    break;
            }
        }
    }

    @SuppressWarnings("deprecation")
    public boolean isLocationEnabled(){
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            String locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            boolean isGpsOn = locationManager.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER);

            return !(locationProviders == null || locationProviders.equals("")) && isGpsOn;

        } else {
            try {
                int locationState = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
                return locationState == Settings.Secure.LOCATION_MODE_HIGH_ACCURACY;
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
