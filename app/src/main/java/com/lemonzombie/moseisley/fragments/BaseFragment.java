package com.lemonzombie.moseisley.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;

import com.google.android.gms.common.api.GoogleApiClient;
import com.lemonzombie.moseisley.App;
import com.lemonzombie.moseisley.managers.RestManager;
import com.lemonzombie.moseisley.utils.RxBus;

import javax.inject.Inject;

/**
 * Created by Adrian on 13.12.2015.
 */
public class BaseFragment extends Fragment {
    private static final String TAG = BaseFragment.class.getSimpleName();

    @Inject
    Context context;

    @Inject
    RxBus rxBus;

    @Inject
    RestManager restManager;

    @Inject
    GoogleApiClient googleApiClient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App)getActivity().getApplication()).getComponent().inject(this);
    }

}
