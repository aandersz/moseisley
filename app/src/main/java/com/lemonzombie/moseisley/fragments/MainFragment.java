package com.lemonzombie.moseisley.fragments;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.lemonzombie.moseisley.R;
import com.lemonzombie.moseisley.events.ReceivedLocationEvent;
import com.lemonzombie.moseisley.events.SearchResponseError;
import com.lemonzombie.moseisley.models.SearchItem;
import com.lemonzombie.moseisley.models.network.SearchResponse;
import com.lemonzombie.moseisley.models.network.Venue;
import com.lemonzombie.moseisley.network.EndpointService;
import com.lemonzombie.moseisley.utils.Const;
import com.lemonzombie.moseisley.views.adapters.SearchVenuesAdapter;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Adrian on 13.12.2015.
 */
public class MainFragment extends BaseFragment {
    private static final String TAG = MainFragment.class.getSimpleName();

    @Bind(R.id.search_box)
    EditText search;

    @Bind(R.id.search_lv)
    ListView listView;

    @Bind(R.id.message)
    TextView message;

    @Bind(R.id.loading)
    ProgressBar loading;

    Location location;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);

        rxBus.register(ReceivedLocationEvent.class)
                .filter(event -> event!=null)
                .subscribe(event -> {
                    location = event.getLocation();
                });

        rxBus.register(SearchResponseError.class)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(event -> {
                    message.setVisibility(View.VISIBLE);
                    loading.setVisibility(View.INVISIBLE);
                    message.setText(event.getError());
                });

        rxBus.register(SearchResponse.class)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .filter(searchResponse -> searchResponse!=null)
                .flatMap(event -> Observable.<Venue>from(event.getResponse().getVenues())
                        .map(v -> new SearchItem(v.getName(), v.getLocation().getAddress(), v.getLocation().getDistance().toString()))
                        .toList())
                .subscribe(event -> {
                    loading.setVisibility(View.INVISIBLE);
                    if (event.size() == 0) {
                        message.setVisibility(View.VISIBLE);
                        message.setText(getString(R.string.message_no_items));
                    }
                    listView.setAdapter(new SearchVenuesAdapter(context, R.layout.search_item, event));
                });

        RxTextView.textChangeEvents(search)
                .debounce(1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(e -> {
                    if (location != null) {
                        loading.setVisibility(View.VISIBLE);
                        message.setVisibility(View.INVISIBLE);
                        runSearchQuery(location, e.text().toString());
                    } else {
                        message.setVisibility(View.VISIBLE);
                        message.setText(getString(R.string.location_problems));
                    }
                });

        return view;
    }

    private void runSearchQuery(Location location, String query){
        Map<String, String> requestMap = new HashMap<>();

        requestMap.put(EndpointService.CLIENT_ID, Const.CLIENT_ID);
        requestMap.put(EndpointService.CLIENT_SECRET, Const.CLIENT_SECRET);
        requestMap.put(EndpointService.LAT_LON, location.getLatitude() + "," + location.getLongitude());
        requestMap.put(EndpointService.VERSION, Const.VERSION);
        requestMap.put(EndpointService.QUERY, query);

        restManager.search(requestMap);
    }

}
