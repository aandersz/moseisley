package com.lemonzombie.moseisley;

import android.content.Context;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lemonzombie.moseisley.network.EndpointService;
import com.lemonzombie.moseisley.utils.RxBus;
import com.squareup.okhttp.OkHttpClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by Adrian on 12.12.2015.
 */
@Module
public class AppModule {
    private App application;
    private Context context;

    public AppModule(App application){
        this.application = application;
        this.context = application.getApplicationContext();
    }

    @Provides
    @Singleton
    public Context provideContext(){
        return context;
    }

    @Provides
    @Singleton
    GoogleApiClient provideGoogleApiClient(Context context){
        return new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .build();
    }

    @Provides
    @Singleton
    public Gson provideGson(){
        return new GsonBuilder()
                .create();
    }

    @Provides
    @Singleton
    OkClient provideOkHttpClient(){
        OkHttpClient okHttpClient = new OkHttpClient();
        return new OkClient(okHttpClient);
    }

    @Provides
    @Singleton
    EndpointService provideEndpointService(OkClient client, Gson gson){

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(EndpointService.URL)
                .setClient(client)
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .setConverter(new GsonConverter(gson))
                .build();

        return restAdapter.create(EndpointService.class);
    }

    @Provides
    @Singleton
    public RxBus provideRxBus(){
        return new RxBus();
    }
}
