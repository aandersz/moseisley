package com.lemonzombie.moseisley.events;

/**
 * Created by Adrian on 31.01.2016.
 */
public class SearchResponseError {
    String error;

    public SearchResponseError(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }
}
