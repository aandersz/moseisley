package com.lemonzombie.moseisley.events;

import android.location.Location;

/**
 * Created by Adrian on 28.01.2016.
 */
public class ReceivedLocationEvent {
    Location location;

    public ReceivedLocationEvent(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }
}
