package com.lemonzombie.moseisley.utils;

import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

/**
 * Created by Adrian on 28.01.2016.
 */
public class RxBus {

    private final Subject<Object, Object> bus = new SerializedSubject<>(PublishSubject.create());

    public void send(Object eventType){
        bus.onNext(eventType);
    }

    public <T extends Object> Observable<T> register(final Class<T> eventType){
        return bus.filter(o -> eventType.isInstance(o))
                .cast(eventType);
    }
}